'use strict';

const assert = require('assert'),
  co = require('co'),
  repoConfig = require('../common/config'),
  TrocResponseHelper = require('../../lib/troc-response-helper'),
  dependencies = {
    logger: {
      log: () => {}
    }
  };

describe('TROC Response Helper Test,', function () {
  it('parseSalesOrderStatus Success test', function (done) {
    co(function* () {
      let trocResponseHelper = new TrocResponseHelper(dependencies, repoConfig);
      let statusLogs = [{
          "UpdatedBy": "Rq.SalesOrderIntegration",
          "Status": "Success",
          "Details": "Successfully saved RQ Sales Order Mapping.",
          "Id": "ccf7b0d1-2cb8-494a-8e0e-89f8c07a4f4d",
          "UpdatedDate": "2019-04-24T09:30:08.84",
          "SaleOrderId": "8fa93b78-d0df-4c53-b94c-26345129d1cf",
          "UserId": 0
        },
        {
          "UpdatedBy": "Rq.SalesOrderIntegration",
          "Status": "Success",
          "Details": "Successfully enqueued Email Sales Order command.",
          "Id": "f6b94247-8695-4fa1-91fa-8e686ad9eec7",
          "UpdatedDate": "2019-04-24T09:30:08.887",
          "SaleOrderId": "8fa93b78-d0df-4c53-b94c-26345129d1cf",
          "UserId": 0
        },
        {
          "UpdatedBy": "Rq.SalesOrderIntegration",
          "Status": "Success",
          "Details": "Successfully sent email",
          "Id": "d5434b51-86dc-4100-b2e3-b280cdd71f49",
          "UpdatedDate": "2019-04-24T09:30:12.853",
          "SaleOrderId": "8fa93b78-d0df-4c53-b94c-26345129d1cf",
          "UserId": 0
        },
        {
          "UpdatedBy": "Rq.SalesOrderIntegration",
          "Status": "Success",
          "Details": "Sales Order successfully sent to RQ (SM001OE745)",
          "Id": "3aca22e0-a67b-41ec-a32a-c67b6447ac62",
          "UpdatedDate": "2019-04-24T09:30:07.947",
          "SaleOrderId": "8fa93b78-d0df-4c53-b94c-26345129d1cf",
          "UserId": 0
        }
      ];
      let status = trocResponseHelper.parseSalesOrderStatus(statusLogs);
      assert.equal(status, 'Success');
      done();
    }).catch((err) => {
      done(err);
    })
  });

  it('parseSalesOrderStatus Pending test', function (done) {
    co(function* () {
      let trocResponseHelper = new TrocResponseHelper(dependencies, repoConfig);
      let statusLogs = [{
          "UpdatedBy": "Rq.SalesOrderIntegration",
          "Status": "Success",
          "Details": "Successfully saved RQ Sales Order Mapping.",
          "Id": "ccf7b0d1-2cb8-494a-8e0e-89f8c07a4f4d",
          "UpdatedDate": "2019-04-24T09:30:08.84",
          "SaleOrderId": "8fa93b78-d0df-4c53-b94c-26345129d1cf",
          "UserId": 0
        },
        {
          "UpdatedBy": "Rq.SalesOrderIntegration",
          "Status": "Success",
          "Details": "Successfully enqueued Email Sales Order command.",
          "Id": "f6b94247-8695-4fa1-91fa-8e686ad9eec7",
          "UpdatedDate": "2019-04-24T09:30:08.887",
          "SaleOrderId": "8fa93b78-d0df-4c53-b94c-26345129d1cf",
          "UserId": 0
        },
        {
          "UpdatedBy": "Rq.SalesOrderIntegration",
          "Status": "Success",
          "Details": "Successfully sent email",
          "Id": "d5434b51-86dc-4100-b2e3-b280cdd71f49",
          "UpdatedDate": "2019-04-24T09:30:12.853",
          "SaleOrderId": "8fa93b78-d0df-4c53-b94c-26345129d1cf",
          "UserId": 0
        }
      ];
      let status = trocResponseHelper.parseSalesOrderStatus(statusLogs);
      assert.equal(status, 'Pending');
      done();
    }).catch((err) => {
      done(err);
    })
  });
});