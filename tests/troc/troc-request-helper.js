'use strict';

const assert = require('assert'),
  co = require('co'),
  repoConfig = require('../common/config'),
  TrocRequestHelper = require('../../lib/troc-request-helper'),
  dependencies = { logger: { log: () => { } } };

describe('TROC Request Helper Test,', function () {
  it('getAuthenticatePayload test', function (done) {
    co(function* () {
      let trocRequestHelper = new TrocRequestHelper(dependencies, repoConfig);
      let authPayload = trocRequestHelper.getAuthenticatePayload();
      let expected = {
        "grant_type": "password",
        "username": repoConfig.trocConfig.username,
        "password": repoConfig.trocConfig.password,
        "client_id": repoConfig.trocConfig.clientId,
        "client_secret": repoConfig.trocConfig.clientSecret
      }
      assert.deepEqual(authPayload, expected);
      done();
    }).catch((err) => {
      done(err);
    })
  });

  it('getCustomerCreationPayload test', function (done) {
    co(function* () {
      let trocRequestHelper = new TrocRequestHelper(dependencies, repoConfig);
      let shipToAddress = {
        ship_to_first_name: "First",
        ship_to_last_name: "Last",
        ship_to_phone: "9876543210",
        ship_to_email: "test@email.com"
      };
      let options = {
        billing_info: {
          line1: "line 1",
          line2: "line 2",
          city: "california",
          country: "US",
          postal_code: "12345",
          state_or_province: "CA"
        }
      };
      let customerCreationPayload = trocRequestHelper.getCustomerCreationPayload(shipToAddress, options);
      let expected = {
        "CustomerTypeId": 2,
        "PrimaryName": "First",
        "FamilyName": "Last",
        "DoNotContact": false,
        "Addresses": [{
          "AddressTypeId": 3,
          "Default": true,
          "DoNotContact": false,
          "CountryCode": "US",
          "Locality": "california",
          "StateCode": "CA",
          "PostalCode": "12345",
          "StreetAddress1": 'line 1, line 2'
        }],
        "ContactMethods": [{
          "ContactMethodCategoryId": 1,
          "ContactMethodTypeId": 3,
          "DoNotContact": false,
          "Value": "9876543210",
          "Default": true
        },
        {
          "ContactMethodCategoryId": 2,
          "ContactMethodTypeId": 9,
          "DoNotContact": false,
          "Value": "test@email.com",
          "Default": true
        }
      ]};
      assert.deepEqual(customerCreationPayload, expected);
      done();
    }).catch((err) => {
      done(err);
    })
  });

  it('getSalesOrderPayload test', function (done) {
    co(function* () {
      let trocRequestHelper = new TrocRequestHelper(dependencies, repoConfig);
      let poInfo = {
        po_id: 'DK1045DE_1'
      };
      let productLines = [
        {
          line_item_number: 1,
          sku: '9df939db-bf76-40d6-a636-434a1c764d5f',
          qty: 1
        },
        {
          line_item_number: 2,
          sku: 'b47424ec-1b5a-42fc-b06d-28404d49a26c',
          qty: 1
        }
      ];
      let customerInfo = {
        customer_id: '350828a1-c8b4-47df-8050-669d165e4b2a',
        address_id: '270ed3e3-7d2c-446b-bb8e-f331b4fdb7e0'
      };
      let options = {
        external_store_id: 201674,
        dr_order_id: '2344324312'
      };
      let soPayload = trocRequestHelper.getSalesOrderPayload(poInfo, productLines, customerInfo, options);
      let expected = {
        "DateCreatedUtc": "2019-04-12T16:00:00",
        "CompanyId": 205156,
        "LocationId": 201674,
        "CustomerId": "350828a1-c8b4-47df-8050-669d165e4b2a",
        "Items": [{
            "LineNumber": 1,
            "ProductCatalogId": "9df939db-bf76-40d6-a636-434a1c764d5f",
            "Quantity": 1,
            "Price": 0.01,
            "PricingTermId": null,
            "CorrelationId": "2344324312",
            "ItemType": "InStock",
            "SerialNumber": null,
            "TaxLineItems": []
          },
          {
            "LineNumber": 2,
            "ProductCatalogId": "b47424ec-1b5a-42fc-b06d-28404d49a26c",
            "Quantity": 1,
            "Price": 0,
            "PricingTermId": null,
            "CorrelationId": "2344324312",
            "ItemType": "InStock",
            "SerialNumber": null,
            "TaxLineItems": []
          }
        ],
        "Payments": [],
        "TaxCalculationResultId": "00000000-0000-0000-0000-000000000000",
        "BillingAddressId": "270ed3e3-7d2c-446b-bb8e-f331b4fdb7e0",
        "ShippingAddressId": "270ed3e3-7d2c-446b-bb8e-f331b4fdb7e0",
        "PrintableId": "DK1045DE_1",
        "UserId": 1958453,
        "ShippingLineItems": [],
        "DiscountLineItems": []
      }
      expected.DateCreatedUtc = soPayload.DateCreatedUtc;
      assert.deepEqual(soPayload, expected);
      done();
    }).catch((err) => {
      done(err);
    })
  });

  it('getProductDetailsPayload test', function (done) {
    co(function* () {
      let trocRequestHelper = new TrocRequestHelper(dependencies, repoConfig);
      let catalogIds = [
        "86ede2a0-7b5a-44dc-a154-5e05c2a5edcc",
        "54ff8936-f9b9-4a9c-9c53-6da8d686481a",
        "218b45c2-7a81-4d37-bc40-7f0c51a04d0a",
        "8bd3a371-5490-479c-a45b-7f48b91c612e"
      ];
      let productDetailsPayload = trocRequestHelper.getProductDetailsPayload(catalogIds);
      let expected = {
        "CatalogItemIds": catalogIds
      }
      assert.deepEqual(productDetailsPayload, expected);
      done();
    }).catch((err) => {
      done(err);
    })
  });
});