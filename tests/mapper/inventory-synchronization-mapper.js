const assert = require('assert'),
  co = require('co'),
  _ = require('lodash'),
  repoConfig = require('../common/config'),
  InventorySynchronizationMapper = require('../../lib/mapper/inventory-synchronization-mapper'),
  InventoryResponseData = require('./data/inventory-response.json'),
  dependencies = {
    logger: {
      log: () => {}
    }
  };

describe('Inventory sync mapper Test,', function () {
  it('map test', function (done) {
    co(function* () {
      let inventorySynchronizationMapper = new InventorySynchronizationMapper(dependencies, repoConfig);
      let mappedInventoryResponse = inventorySynchronizationMapper.map(InventoryResponseData, {store_id: 8001});
      let expected = {
        fulfiller_info: {
          partner_name: 'troc'
        }
      };
      expected.line_items = _.map(InventoryResponseData, function(inventory) {
        return {
          fulfiller_sku: inventory.ProductId,
          warehouse_id: 8001,
          fulfiller_store_id: inventory.EntityId,
          quantity_available: _.max([(inventory.Quantity - inventory.MinQuantity), 0]),
          quantity_on_order: inventory.QuantityOnOrder
        };
      });
      mappedInventoryResponse.line_items = _.map(mappedInventoryResponse.line_items, function(inventory) {
        return _.omit(inventory, 'sync_timestamp');
      });
      assert.deepEqual(mappedInventoryResponse, expected);
      done();
    }).catch((err) => {
      done(err);
    })
  });
});