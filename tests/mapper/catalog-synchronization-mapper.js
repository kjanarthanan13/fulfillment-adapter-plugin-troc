'use strict';

const assert = require('assert'),
  co = require('co'),
  _ = require('lodash'),
  repoConfig = require('../common/config'),
  CatalogSynchronizationMapper = require('../../lib/mapper/catalog-synchronization-mapper'),
  CatalogResponseData = require('./data/catalog-product-details-response.json'),
  dependencies = {
    logger: {
      log: () => {}
    }
  };

describe('Catalog sync mapper Test,', function () {
  it('map test', function (done) {
    co(function* () {
      let catalogSynchronizationMapper = new CatalogSynchronizationMapper(dependencies, repoConfig);
      let mappedCatalogResponse = catalogSynchronizationMapper.map(CatalogResponseData);
      let expected = [{
          "ManufacturerSkus": [{
            "Value": "SM-N960UZBATMB",
            "Description": "",
            "Entity": {
              "Id": 12883,
              "Name": "Samsung"
            }
          }],
          catalog_id: "899df593-40ec-4716-a011-06a40cb0d366",
        },
        {
          "ManufacturerSkus": [{
            "Value": "SM-G970UZKATMB",
            "Description": "",
            "Entity": {
              "Id": 12883,
              "Name": "Samsung"
            }
          }],
          catalog_id: "26014f03-4d99-4a17-9e26-123146a90cb3",
        }
      ];
      mappedCatalogResponse = _.map(mappedCatalogResponse, function(catalog) {
        return _.omit(catalog, 'sync_timestamp');
      });
      assert.deepEqual(mappedCatalogResponse, expected);
      done();
    }).catch((err) => {
      done(err);
    })
  });

  it('mapCatalogIds test', function (done) {
    co(function* () {
      let catalogSynchronizationMapper = new CatalogSynchronizationMapper(dependencies, repoConfig);
      let CatalogResponseData = [
        {
            "RmsId": "2581",
            "Slug": "M17047-V1-E12337",
            "CatalogItemId": "899df593-40ec-4716-a011-06a40cb0d366",
            "CatalogSku": "FKH8QF7C",
            "MeasurementType": "SingleUnit",
            "BatchTracking": false,
            "NonStock": false,
            "LifeCycle": "Active",
            "IsArchived": false,
            "SourceIds": [
                "00000000-0000-0000-0000-000000000000"
            ],
            "DateAddedUtc": "2019-04-02T18:53:50.51Z",
            "DateUpdatedUtc": "2019-04-02T18:53:50.51Z"
        },
        {
            "RmsId": "2764",
            "Slug": "M17844-V1-E12337",
            "CatalogItemId": "26014f03-4d99-4a17-9e26-123146a90cb3",
            "CatalogSku": "D2N77XY7",
            "MeasurementType": "SingleUnit",
            "BatchTracking": false,
            "NonStock": false,
            "LifeCycle": "Active",
            "IsArchived": false,
            "SourceIds": [
                "00000000-0000-0000-0000-000000000000"
            ],
            "DateAddedUtc": "2019-04-02T18:52:09.28Z",
            "DateUpdatedUtc": "2019-04-02T18:52:09.28Z"
        }];
      let mappedCatalogResponse = catalogSynchronizationMapper.mapCatalogIds(CatalogResponseData);
      let expected = [
        "899df593-40ec-4716-a011-06a40cb0d366",
        "26014f03-4d99-4a17-9e26-123146a90cb3"
      ];
      assert.deepEqual(mappedCatalogResponse, expected);
      done();
    }).catch((err) => {
      done(err);
    })
  });
});