const assert = require('assert'),
  co = require('co'),
  _ = require('lodash'),
  repoConfig = require('../common/config'),
  OrderResponseMapper = require('../../lib/mapper/order-response-mapper'),
  dependencies = {
    logger: {
      log: () => {}
    }
  };

describe('Order response mapper Test,', function () {
  it('map test', function (done) {
    co(function* () {
      let orderResponseMapper = new OrderResponseMapper(dependencies, repoConfig);
      let orderData = {
        "Id": "8fa93b78-d0df-4c53-b94c-26345129d1cf",
        "DateCreatedUtc": "2019-04-12T16:00:00",
        "DateInsertedUtc": "2019-04-24T09:29:52.2757051Z",
        "DateUpdatedUtc": "2019-04-24T09:29:52.2757051Z",
        "CompanyId": 205156,
        "LocationId": 205167,
        "CustomerId": "4274c51c-fb14-46f7-97a2-550ee41e709e",
        "ExpiryDateUtc": null,
        "Items": [{
          "LineNumber": 1,
          "ProductCatalogId": "5c83b537-7ae9-4732-a74d-31f594b1351f",
          "Quantity": 1,
          "Price": 0.01,
          "PricingTermId": null,
          "CorrelationId": "123456789",
          "ItemType": "InStock",
          "SupplierEntityId": null,
          "SerialNumber": null,
          "TaxLineItems": []
        }],
        "Payments": [],
        "TaxCalculationResultId": "00000000-0000-0000-0000-000000000000",
        "BillingAddressId": "3c464dea-016d-4321-80fd-da3dd7b9d82d",
        "ShippingAddressId": "3c464dea-016d-4321-80fd-da3dd7b9d82d",
        "PrintableId": "KH20190225_2",
        "DropshipOrderId": "00000000-0000-0000-0000-000000000000",
        "UserId": 1958453,
        "ShippingLineItems": [],
        "DiscountLineItems": []
      };

      let mappedOrderResponse = orderResponseMapper.map(orderData);
      let expected = {
        is_success: true,
        is_status_check_required: true,
        so_id: orderData.Id,
        fulfillment_po_id: orderData.PrintableId,
        fulfiller_info: {
          partner_name: 'troc'
        },
        line_items: [{
          line_id: 1,
          so_line_id: '1',
          fulfiller_sku: '5c83b537-7ae9-4732-a74d-31f594b1351f',
          quantity: 1
        }]
      };
      expected.fulfiller_attributes = orderData;
      assert.deepEqual(mappedOrderResponse, expected);
      done();
    }).catch((err) => {
      done(err);
    })
  });
});