"use strict";
const BaseHelper = require('ecom-base-lib').BaseHelper,
  _ = require('lodash');

class CatalogSynchronizationMapper extends BaseHelper {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
  }
  map(catalogData) {
    let returnValue = [];
    if (!_.isEmpty(catalogData) && !_.isEmpty(catalogData.CatalogItems)) {
      _.forEach(catalogData.CatalogItems, function(value, key) {
        if (!_.isEmpty(_.get(value, 'ManufacturerSkus[0].Value'))) {
          let lineItem = _.pick(value, ['ManufacturerSkus']);
          lineItem.catalog_id = key;
          lineItem.sync_timestamp = new Date();
          returnValue.push(lineItem);
        }
      });
    }
    return returnValue;
  }
  mapCatalogIds(catalogData) {
    let returnValue = [];
    if (!_.isEmpty(catalogData)) {
      _.forEach(catalogData, function(catalog) {
        let catalogId = _.get(catalog, 'CatalogItemId');
        returnValue.push(catalogId);
      });
    }
    return returnValue;
  }
}

module.exports = CatalogSynchronizationMapper;
