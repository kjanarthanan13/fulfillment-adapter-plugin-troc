"use strict";
const BaseHelper = require('ecom-base-lib').BaseHelper,
  _ = require('lodash');

class OrderResponseMapper extends BaseHelper {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
  }
  map(orderData) {
    let returnValue = {
      is_success: true,
      is_status_check_required: true,
      so_id: orderData.Id,
      fulfillment_po_id: orderData.PrintableId,
      fulfiller_info: {
        partner_name: 'troc'
      }
    };

    if (orderData.Items) {
      returnValue.line_items = _.map(orderData.Items, function(lineItem) {
        let item = {
          line_id: lineItem.LineNumber,
          so_line_id: _.toString(lineItem.LineNumber),
          fulfiller_sku: lineItem.ProductCatalogId,
          quantity: lineItem.Quantity
        };
        return _.omitBy(item, _.isNil);
      });
    }
    returnValue.fulfiller_attributes = orderData;
    return returnValue;
  }
}

module.exports = OrderResponseMapper;