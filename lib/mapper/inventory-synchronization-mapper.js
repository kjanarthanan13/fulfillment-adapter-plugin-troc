"use strict";
const BaseHelper = require('ecom-base-lib').BaseHelper,
  _ = require('lodash');

class InventorySynchronizationMapper extends BaseHelper {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
  }
  map(inventoryData, options) {
    let returnValue = {
      fulfiller_info: {
        partner_name: 'troc'
      }
    };

    returnValue.line_items = [];
    if (!_.isEmpty(inventoryData)) {
      for (let j = 0; j < inventoryData.length; j++) {
        let inventory = inventoryData[j];
        let lineItem = {
          fulfiller_sku: _.get(inventory, 'ProductId'),
          warehouse_id: options.store_id,
          fulfiller_store_id: _.get(inventory, 'EntityId'),
          // BOPIS Availability = Quantity - MinQuantity (<= 0 is out of stock)
          quantity_available: _.max([(_.get(inventory, 'Quantity') - _.get(inventory, 'MinQuantity')), 0]),
          quantity_on_order: _.get(inventory, 'QuantityOnOrder'),
          sync_timestamp: new Date()
        };
        returnValue.line_items.push(lineItem);
      }
    }

    return returnValue;
  }
}

module.exports = InventorySynchronizationMapper;
