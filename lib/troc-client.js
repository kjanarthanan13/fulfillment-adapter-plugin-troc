"use strict";

const co = require('co'),
  BaseServiceClient = require('ecom-base-lib').BaseServiceClient,
  InventorySynchronizationMapper = require('./mapper/inventory-synchronization-mapper'),
  CatalogSynchronizationMapper = require('./mapper/catalog-synchronization-mapper'),
  OrderResponseMapper = require('./mapper/order-response-mapper'),
  TROCRequestHelper = require('./troc-request-helper'),
  TROCResponseHelper = require('./troc-response-helper'),
  repoInfo = require('../repo-info.js'),
  _ = require('lodash'),
  scope = `TROCClient#${repoInfo.version}`;

class TROCClient extends BaseServiceClient {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
    this.trocRequestHelper = new TROCRequestHelper(dependencies, config, requestContext);
    this.trocResponseHelper = new TROCResponseHelper(dependencies, config, requestContext);
  }

  getHeader() {
    const me = this;
    return co(function* () {
      const accessToken = yield me.getAccessToken();
      return {
        "Authorization": "Bearer " + accessToken,
        "Accept": "application/json",
        "Content-Type": "application/json"
      }
    }).catch(err => {
      me.errorv2(scope, 'getHeader', err);
      throw err;
    });
  }

  authenticate() {
    const me = this;
    return co(function* () {
      const requestPayload = me.trocRequestHelper.getAuthenticatePayload();
      const headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      me.logv2(scope, 'authenticate', 'Getting access token');
      const response = yield me._postForm(me.config.trocConfig.accountServiceUrl + "/oauth2/token", headers, requestPayload);
      return response;
    }).catch(err => {
      me.errorv2(scope, 'authenticate', err);
      throw err;
    });
  }

  getAccessToken() {
    const me = this;
    return co(function* () {
      let cachedResponse = yield me.dependencies.redisClient.get('fulfillment_troc_auth');
      cachedResponse = JSON.parse(cachedResponse);
      if (!_.isEmpty(cachedResponse)) {
        return cachedResponse.access_token;
      }
      me.logv2(scope, 'getAccessToken', 'Access token cache miss');
      const authResponse = yield me.authenticate();
      // set cache ttl as (expires_in - 5mins)
      yield me.dependencies.redisClient.set('fulfillment_troc_auth', JSON.stringify(authResponse), authResponse.expires_in - 300);
      return authResponse.access_token;
    }).catch(err => {
      me.errorv2(scope, 'getAccessToken', err);
      throw err;
    });
  }

  createOrder(poInfo, carrierCode, shipToAddress, productLines, options) {
    const me = this;
    return co(function* () {
      let headers = yield me.getHeader();
      let customerCreationPayload = me.trocRequestHelper.getCustomerCreationPayload(shipToAddress, options);
      const customerResponse = yield me._createCustomer(customerCreationPayload);
      let customerInfo = {
        "customer_id": customerResponse.Id,
        "address_id": _.get(customerResponse, 'Addresses[0].Id')
      };
      let requestPayload = me.trocRequestHelper.getSalesOrderPayload(poInfo, productLines, customerInfo, options);
      me.logv2(scope, 'createOrder', 'Sending this data to TROC', { requestPayload });
      const response = yield me._postBody(me.config.trocConfig.salesOrderServiceUrl + `/Companies(${me.config.trocConfig.companyId})/SaleOrder`, headers, requestPayload);
      const orderResponseMapper = new OrderResponseMapper(me.dependencies, me.config, me.rawRequestContext);
      const mappedResponse = orderResponseMapper.map(response);
      return mappedResponse;
    }).catch(err => {
      me.errorv2(scope, 'createOrder', err);
      throw err;
    });
  }

  _createCustomer(requestPayload) {
    const me = this;
    return co(function* () {
      let headers = yield me.getHeader();
      me.logv2(scope, '_createCustomer', 'Sending this data to TROC', { requestPayload });
      const response = yield me._postBody(me.config.trocConfig.crmServiceUrl + `/Companies(${me.config.trocConfig.companyId})/CustomerFull`, headers, requestPayload);
      return response;
    }).catch(err => {
      me.errorv2(scope, 'authenticate', err);
      throw err;
    });
  }

  inventoryCheck(skus, options) {
    const me = this;
    me.logv2(scope, 'inventoryCheck', 'Sending this data to TROC', { options });
    return co(function* () {
      const top = me.config.trocConfig.inventoryPullBatchSize || 50;
      let skip = 0;
      let reachedEnd = false;
      let inventoryResponse = [];
      while(!reachedEnd) {
        const response = yield me._getAvailabilityBelowThreshold(options, skip, top);
        if (_.size(response) > 0) {
          inventoryResponse = _.concat(inventoryResponse, response);
          skip += top;
        }
        else {
          reachedEnd = true;
        }
      }
      const inventorySynchronizationMapper = new InventorySynchronizationMapper(me.dependencies, me.config, me.rawRequestContext);
      const mappedResponse = inventorySynchronizationMapper.map(inventoryResponse, options);
      return mappedResponse;
    }).catch(err => {
      me.errorv2(scope, 'inventoryCheck', err);
      throw err;
    });
  }

  _getAvailabilityBelowThreshold(options, skip, top) {
    const me = this;
    me.logv2(scope, '_getAvailabilityBelowThreshold', 'Sending this data to TROC - Pagination', { skip, top });
    return co(function* () {
      let headers = yield me.getHeader();
      const url = `${me.config.trocConfig.availabilityServiceUrl}/Companies(${me.config.trocConfig.companyId})/Entities(${options.fulfiller_store_id})/AvailabilityBelowThreshold?$top=${top}&$skip=${skip}`;
      const response = yield me._get(url, headers);
      return response;
    }).catch(err => {
      me.errorv2(scope, '_getAvailabilityBelowThreshold', err);
      throw err;
    });
  }

  getCatalogMappings() {
    const me = this;
    me.logv2(scope, 'getCatalogMappings', 'calling TROC to get sku mappings');
    return co(function* () {
      let availableCatalogIds = yield me._getAllCatalogItems();
      let catalogListChunk = _.chunk(availableCatalogIds, me.config.trocConfig.catalogPullBatchSize || 50);
      let catalogResponse = [];
      const catalogSynchronizationMapper = new CatalogSynchronizationMapper(me.dependencies, me.config, me.rawRequestContext);
      for (let i = 0; i < catalogListChunk.length; i++) {
        try {
          let headers = yield me.getHeader();
          let requestPayload = me.trocRequestHelper.getProductDetailsPayload(catalogListChunk[i]);
          const url = `${me.config.trocConfig.catalogServiceUrl}/Companies(${me.config.trocConfig.companyId})/Catalog/Items/ProductDetails/Bulk`;
          const response = yield me._postBody(url, headers, requestPayload);
          const mappedResponse = catalogSynchronizationMapper.map(response);
          catalogResponse = _.concat(catalogResponse, mappedResponse);
        }
        catch (err) {
          me.errorv2(scope, 'getCatalogMappings', err);
        }
      }
      return catalogResponse;
    }).catch(err => {
      me.errorv2(scope, 'getCatalogMappings', err);
      throw err;
    });
  }

  _getAllCatalogItems() {
    const me = this;
    me.logv2(scope, '_getAllCatalogItems', 'calling TROC to get all available catalog items');
    return co(function* () {
      let headers = yield me.getHeader();
      const url = `${me.config.trocConfig.catalogServiceUrl}/Companies(${me.config.trocConfig.companyId})/Catalog/Items`;
      const response = yield me._get(url, headers);
      const catalogSynchronizationMapper = new CatalogSynchronizationMapper(me.dependencies, me.config, me.rawRequestContext);
      const catalogIds = catalogSynchronizationMapper.mapCatalogIds(response);
      return catalogIds;
    }).catch(err => {
      me.errorv2(scope, '_getAllCatalogItems', err);
      throw err;
    });
  }

  getOrderSubmissionStatusById(orderId) {
    const me = this;
    me.logv2(scope, 'getOrderSubmissionStatusById', 'calling TROC to get order status', { orderId });
    return co(function* () {
      let headers = yield me.getHeader();
      const url = `${me.config.trocConfig.salesOrderServiceUrl}/Companies(${me.config.trocConfig.companyId})/SaleOrder(${orderId})/StatusLog`;
      const response = yield me._get(url, headers);
      return me.trocResponseHelper.parseSalesOrderStatus(response);
    }).catch(err => {
      me.errorv2(scope, 'getOrderSubmissionStatusById', err);
      throw err;
    });
  }

  // putProductsOnHold(salesOrder) {
  //   const me = this;
  //   me.logv2(scope, 'putProductsOnHold', 'calling TROC to put products on hold', { salesOrder });
  //   return co(function* () {
  //     let headers = yield me.getHeader();
  //     let requestPayload = me.trocRequestHelper.getHoldProductsPayload(salesOrder);
  //     const url = `${me.config.trocConfig.availabilityServiceUrl}/Companies(${me.config.trocConfig.companyId})/ProductHoldBatch`;
  //     const response = yield me._postBody(url, headers, requestPayload);
  //     me.logv2(scope, 'putProductsOnHold', 'TROC hold response', { requestPayload,  response });
  //     return me.trocResponseHelper.parseHoldProductsResponse(response);
  //   }).catch(err => {
  //     me.errorv2(scope, 'putProductsOnHold', err);
  //     throw err;
  //   });
  // }

  // deleteHoldBatch(holdBatchId) {
  //   const me = this;
  //   me.logv2(scope, 'deleteHoldBatch', 'calling TROC to release hold', { holdBatchId });
  //   return co(function* () {
  //     let headers = yield me.getHeader();
  //     const url = `${me.config.trocConfig.availabilityServiceUrl}/Companies(${me.config.trocConfig.companyId})/ProductHoldBatch(${holdBatchId})`;
  //     yield me._delete(url, headers);
  //     return true;
  //   }).catch(err => {
  //     me.errorv2(scope, 'deleteHoldBatch', err);
  //     throw err;
  //   });
  // }
}

module.exports = TROCClient;
