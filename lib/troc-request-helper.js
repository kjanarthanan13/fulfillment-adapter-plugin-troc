"use strict";

const BaseHelper = require('ecom-base-lib').BaseHelper,
  _ = require('lodash'),
  repoInfo = require('../repo-info.js');

class TrocRequestHelper extends BaseHelper {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
  }

  getAuthenticatePayload() {
    const me = this;
    return {
      "grant_type": "password",
      "username": me.config.trocConfig.username,
      "password": me.config.trocConfig.password,
      "client_id": me.config.trocConfig.clientId,
      "client_secret": me.config.trocConfig.clientSecret
    }
  }

  getSalesOrderPayload(poInfo, productLines, customerInfo, options) {
    const me = this;
    let salesOrderPayload = {
      "DateCreatedUtc": new Date().toISOString(),
      "CompanyId": me.config.trocConfig.companyId,
      "LocationId": options.external_store_id,
      "CustomerId": customerInfo.customer_id,
      "Items": [],
      "Payments": [],
      "TaxCalculationResultId": "00000000-0000-0000-0000-000000000000",
      "BillingAddressId": customerInfo.address_id,
      "ShippingAddressId": customerInfo.address_id,
      "PrintableId": poInfo.po_id,
      "UserId": me.config.trocConfig.userId,
      "ShippingLineItems": [],
      "DiscountLineItems": []
    }

    _.forEach(productLines, function(lineItem) {
      salesOrderPayload.Items.push({
        "LineNumber": lineItem.line_item_number,
        "ProductCatalogId": lineItem.sku,
        "Quantity": lineItem.qty,
        "Price": 0,
        "PricingTermId": null,
        "CorrelationId": options.dr_order_id,
        "ItemType": "InStock",
        "SerialNumber": null,
        "TaxLineItems": []
      });
    });

    return salesOrderPayload
  }

  getCustomerCreationPayload(shipToAddress, options) {
    let customerInfo = {
      "CustomerTypeId": 2,
      "PrimaryName": shipToAddress.ship_to_first_name,
      "FamilyName": shipToAddress.ship_to_last_name,
      "DoNotContact": false
    };
    customerInfo.Addresses = [{
      "AddressTypeId": 3, // Shipping
      "Default": true,
      "DoNotContact": false,
      "CountryCode": _.get(options, 'billing_info.country'),
      "Locality": _.get(options, 'billing_info.city'),
      "StateCode": _.get(options, 'billing_info.state_or_province'),
      "PostalCode": _.get(options, 'billing_info.postal_code'),
      "StreetAddress1": _.truncate(_.get(options, 'billing_info.line2')
        ? _.get(options, 'billing_info.line1') + ', ' + _.get(options, 'billing_info.line2')
        : _.get(options, 'billing_info.line1'), {length: 49, omission: ''})
    }];
    customerInfo.ContactMethods = [];
    customerInfo.ContactMethods.push({
      "ContactMethodCategoryId": 1,
      "ContactMethodTypeId": 3,
      "DoNotContact": false,
      "Value": _.get(shipToAddress, 'ship_to_phone', '').replace(/\D/g, ''), // strip non numeric characters
      "Default": true
    });
    customerInfo.ContactMethods.push({
      "ContactMethodCategoryId": 2,
      "ContactMethodTypeId": 9,
      "DoNotContact": false,
      "Value": shipToAddress.ship_to_email,
      "Default": true
    });

    return customerInfo;
  }

  getProductDetailsPayload(catalogIds) {
    return {
      "CatalogItemIds": catalogIds
    };
  }

  // getHoldProductsPayload(salesOrder) {
  //   const me = this;
  //   if (!_.isEmpty(salesOrder)) {
  //     let holdProductsPayload = {
  //       "EntityId": salesOrder.LocationId,
  //       "HoldDurationInMinutes": me.config.trocConfig.holdDurationInMinutes || 8640,
  //       "Products": []
  //     };

  //     holdProductsPayload.Products = _.map(salesOrder.Items, function(soLineItem) {
  //       return {
  //         "CatalogProductId": soLineItem.ProductCatalogId,
  //         "Quantity": soLineItem.Quantity
  //       }
  //     });

  //     return holdProductsPayload;
  //   }
  // }
}

module.exports = TrocRequestHelper;
