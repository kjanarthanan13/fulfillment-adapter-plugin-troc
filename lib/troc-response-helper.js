"use strict";

const BaseHelper = require('ecom-base-lib').BaseHelper,
  TrocEnum = require('../common/enum'),
  Enum = require('ecom-fulfillment-lib').Enum,
  _ = require('lodash');

class TrocResponseHelper extends BaseHelper {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
  }

  parseSalesOrderStatus(statusLogs) {
    const me = this;
    let status = Enum.FulfillerOrderSubmissionStatus.Pending;
    _.forEach(statusLogs, function(log) {
      if (log.Status === TrocEnum.SalesOrderLogStatus.Success
        && _.startsWith(_.toLower(log.Details), _.toLower(me.config.trocConfig.salesOrderSuccessMessage))) {

        status = Enum.FulfillerOrderSubmissionStatus.Success;
        return false;
      }
    });

    return status;
  }

  // parseHoldProductsResponse(holdResponse) {
  //   return {
  //     "hold_batch_id": holdResponse.Id
  //   }
  // }
}

module.exports = TrocResponseHelper;