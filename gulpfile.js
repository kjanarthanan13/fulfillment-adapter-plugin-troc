"use strict";

const gulp = require('gulp'),
  mocha = require('gulp-mocha'),
  gutil = require('gulp-util'),
  eslint = require('gulp-eslint'),
  istanbul = require('gulp-istanbul'),
  istanbulReport = require('gulp-istanbul-report'),
  del = require('del'),
  coverageConfig = require('./coverage-config.json'),
  fs = require('fs'),
  git = require('gulp-git'),
  bump = require('gulp-bump'),
  filter = require('gulp-filter'),
  tag_version = require('gulp-tag-version');

// Run unit test using mocha
gulp.task('mocha', () => {
  return gulp.src(['tests/**/*.js'], { read: false })
    .pipe(mocha({ reporter: 'list' }))
    .on('error', gutil.log);
});

gulp.task('watch-mocha', () => {
  gulp.watch(['lib/**', 'tests/**'], ['mocha']);
});

// build the main source into the min file
gulp.task('eslint', () => {
  // ESLint ignores files with "node_modules" paths.
  // So, it's best to have gulp ignore the directory as well.
  // Also, Be sure to return the stream from the task;
  // Otherwise, the task may end before the stream has finished.
  return gulp.src(['lib/*/*.js', 'lib/*/*/*.js', 'lib/*/*/*/*.js', 'server.js', 'app.js', "gulpfile.js"])
    // eslint() attaches the lint output to the "eslint" property
    // of the file object so it can be used by other modules.
    .pipe(eslint({
      config: './.eslintrc.json'
    }))
    // eslint.format() outputs the lint results to the console.
    // Alternatively use eslint.formatEach() (see Docs).
    .pipe(eslint.format())
    // To have the process exit with an error code (1) on
    // lint error, return the stream and pipe to failAfterError last.
    .pipe(eslint.failAfterError());
});

gulp.task('check-coverage', function (cb) {
  gulp.src(['lib/**/*.js'])
    .pipe(istanbul()) // Covering files
    .pipe(istanbul.hookRequire()) // Force `require` to return covered files
    .on('finish', () => {
      gulp.src(['tests/**/*.js'])
        .pipe(mocha())
        .pipe(istanbul.writeReports({ reporters: ['html', 'json'] })) // Creating the reports after tests ran
        .pipe(istanbul.enforceThresholds({
          thresholds: {
            global: coverageConfig
          }
        })).on('end', cb);
    });
});

gulp.task('report-coverage', (cb) => {
  return gulp.src('./coverage/coverage-final.json')
    .pipe(istanbulReport({
      reporters: [{ name: 'text-summary', file: '../COVERAGE.md' }]
    })).on('end', function () {
      fs.readFile('COVERAGE.md', 'utf8', function (err, coverageData) {
        coverageData = "```" + coverageData + "\n```"
        fs.writeFile('COVERAGE.md', coverageData, 'utf8', function (err) {
          if (err) return cb(err);
        });
      });
    });
});

gulp.task('clean', cb => {
  del(['./docs'], cb);
});

// Generate the documentation for the internal libraries using jsdoc and put it in ./docs
gulp.task('jsdoc', function (cb) {
  const jsdoc = require('gulp-jsdoc3');
  const config = require('./jsdoc-config');
  gulp.src(['lib/**/*.js'], { read: false })
    .pipe(jsdoc(config, cb))
    .on('error', gutil.log);
});

function inc(importance) {
  // get all the files to bump version in
  return gulp.src(['./package.json'])
    // bump the version number in those files
    .pipe(bump({
      type: importance
    }))
    // save it back to filesystem
    .pipe(gulp.dest('./'))
    // commit the changed version number
    .pipe(git.commit('bumps package version'))

    // read only one file to get the version number
    .pipe(filter('package.json'))
    // **tag it in the repository**
    .pipe(tag_version());
}

gulp.task('patch', function () {
  return inc('patch');
})
gulp.task('feature', function () {
  return inc('minor');
})
gulp.task('release', function () {
  return inc('major');
})

gulp.task('precommit', ['clean', 'eslint', 'jsdoc', 'report-coverage', 'check-coverage']);

// Default task to run eslint, jsdoc, mocha in series.
gulp.task('default', ['clean', 'eslint', 'mocha']);
